use crate::db::{self, DbRef};
use crate::User;
use chrono::{DateTime, Duration, Utc};
use rand::{thread_rng, Rng};
use rocket::{
    form::{self, FromFormField, ValueField},
    http::Status,
    outcome::Outcome,
    request::{self, FromRequest, Request},
    State,
};
use serde::{Serialize, Serializer};
use std::str::FromStr;
use std::{
    collections::HashMap,
    fmt::{self, Display, Formatter},
};

pub const TOKEN_COOKIE: &'static str = "KnowledgeBase-User";

#[derive(Debug, Clone, Serialize)]
#[serde(tag = "type")]
pub enum AuthStatus {
    User(User),
    Anonymous,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for AuthStatus {
    type Error = db::Error;

    async fn from_request(req: &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
        match req
            .cookies()
            .get(TOKEN_COOKIE)
            .and_then(|cookie| HexArray::from_str(cookie.value()).ok())
        {
            None => Outcome::Success(AuthStatus::Anonymous),
            Some(secret) => match req.guard::<&State<DbRef>>().await {
                Outcome::Success(db) => match db.auth_user(secret).await {
                    Ok(Some(user)) => Outcome::Success(AuthStatus::User(user)),
                    Ok(None) => Outcome::Success(AuthStatus::Anonymous),
                    Err(err) => Outcome::Failure((Status::InternalServerError, err)),
                },
                Outcome::Failure((status, ())) => Outcome::Failure((status, db::Error::NotFound)),
                Outcome::Forward(()) => Outcome::Forward(()),
            },
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct HexArray<const N: usize>(pub [u8; N]);

#[derive(Debug)]
// TODO thiserror
pub enum HexParseError {
    InvalidLength { expected: usize, found: usize },
    NonHexadecimal(char),
}

impl<const N: usize> Display for HexArray<N> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        fn hex_digit(x: u8) -> char {
            match x {
                0..=9 => (b'0' + x) as char,
                10..=16 => (b'a' + x - 10) as char,
                _ => unreachable!(),
            }
        }

        for i in 0..N {
            let x = self.0[i];
            write!(f, "{}{}", hex_digit(x >> 4), hex_digit(x & 0xf))?;
        }
        Ok(())
    }
}

impl<const N: usize> FromStr for HexArray<N> {
    type Err = HexParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.len() != 2 * N {
            return Err(HexParseError::InvalidLength {
                expected: 2 * N,
                found: s.len(),
            });
        }

        let mut arr = [0; N];
        for (i, c) in s.chars().enumerate() {
            let nib = match c {
                '0'..='9' => c as u8 - b'0',
                'a'..='f' => 10 + c as u8 - b'a',
                'A'..='F' => 10 + c as u8 - b'F',
                _ => return Err(HexParseError::NonHexadecimal(c)),
            };

            if i % 2 == 0 {
                arr[i / 2] = nib << 4;
            } else {
                arr[i / 2] |= nib;
            }
        }

        Ok(HexArray(arr))
    }
}

impl<const N: usize> Serialize for HexArray<N> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}

impl<const N: usize> FromFormField<'_> for HexArray<N> {
    fn from_value(field: ValueField) -> form::Result<'_, Self> {
        Self::from_str(field.value).map_err(|_| {
            form::Error::validation(format!("must contain {} hexadecimal characters", 2 * N)).into()
        })
    }
}

impl<const N: usize> HexArray<N> {
    pub fn zeros() -> HexArray<N> {
        HexArray([0; N])
    }

    pub fn random() -> HexArray<N> {
        let mut arr = [0; N];
        thread_rng().fill(&mut arr);
        HexArray(arr)
    }
}

pub type ReqId = HexArray<16>;
pub type Secret = HexArray<32>;

#[derive(Debug)]
pub struct Token {
    pub(crate) secret: Secret,
    expiration: DateTime<Utc>,
    // None when the request first arrives but once we verify the secret (before sending to
    // database) this will be Some
    pub(crate) username: Option<String>,
}

const LOGIN_TIMEOUT_SECS: i64 = 300;

#[derive(Debug)]
pub struct LoginManager {
    tokens: HashMap<ReqId, Token>,
}

impl LoginManager {
    pub fn new() -> LoginManager {
        LoginManager {
            tokens: HashMap::new(),
        }
    }

    pub fn start_login(&mut self) -> ReqId {
        let req_id = HexArray::random();
        let secret = HexArray::random();
        println!("secret for request {} is {}", req_id, secret);
        self.tokens.insert(
            req_id,
            Token {
                secret,
                expiration: Utc::now() + Duration::seconds(LOGIN_TIMEOUT_SECS),
                username: None,
            },
        );
        req_id
    }

    pub async fn attempt_login(
        &mut self,
        req_id: ReqId,
        secret: Secret,
        username: String,
        db: &DbRef,
    ) -> Result<Secret, ()> {
        if let Some(mut tok) = self.tokens.remove(&req_id) {
            if secret == tok.secret && Utc::now() <= tok.expiration {
                tok.username = Some(username);
                // TODO handle db error
                db.create_auth_token(tok).await.map_err(|_| ())?;
                Ok(secret)
            } else {
                Err(())
            }
        } else {
            Err(())
        }
    }
}
