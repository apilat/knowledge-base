use crate::auth::{HexArray, Secret, Token};
use crate::{Content, FragId, Fragment, Tags, User};
use chrono::{DateTime, NaiveDateTime, Utc};
use rusqlite::{
    params,
    types::{FromSql, FromSqlResult, ToSqlOutput, Value, ValueRef},
    Connection, ToSql,
};
use std::thread::JoinHandle;
use std::{
    convert::{TryFrom, TryInto},
    path::PathBuf,
};
use tokio::sync::{mpsc, oneshot};

impl ToSql for Tags {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput<'_>> {
        let mut s = String::from(",");
        for t in &self.0 {
            s.push_str(t);
            s.push(',');
        }
        Ok(ToSqlOutput::Owned(Value::Text(s)))
    }
}

impl FromSql for Tags {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        let mut t = Vec::new();
        for s in value.as_str()?.split(',') {
            if !s.is_empty() {
                t.push(s.to_owned());
            }
        }
        Ok(Tags(t))
    }
}

impl ToSql for Content {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput<'_>> {
        use rusqlite::Error::ToSqlConversionFailure;

        let mut bytes = Vec::new();
        bytes.extend_from_slice(
            &u32::try_from(self.title.len())
                .map_err(|_| ToSqlConversionFailure(format!("title length exceeds u32").into()))?
                .to_be_bytes(),
        );
        bytes.extend_from_slice(self.title.as_bytes());

        bytes.extend_from_slice(
            &u32::try_from(self.description.len())
                .map_err(|_| {
                    ToSqlConversionFailure(format!("description length exceeds u32").into())
                })?
                .to_be_bytes(),
        );
        bytes.extend_from_slice(self.description.as_bytes());

        Ok(ToSqlOutput::Owned(Value::Blob(bytes)))
    }
}

impl FromSql for Content {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        use rusqlite::types::FromSqlError::Other;

        let (title, description);
        let mut bytes = value.as_blob()?;
        {
            if bytes.len() < 4 {
                return Err(Other(format!("no title length").into()));
            }
            let len = u32::from_be_bytes(bytes[..4].try_into().unwrap()) as usize;
            bytes = &bytes[4..];

            if bytes.len() < len {
                return Err(Other(format!("no title").into()));
            }
            title = std::str::from_utf8(&bytes[..len])
                .map_err(|_| Other(format!("non-utf8 title").into()))?
                .to_owned();
            bytes = &bytes[len..];
        }

        {
            if bytes.len() < 4 {
                return Err(Other(format!("no description length").into()));
            }
            let len = u32::from_be_bytes(bytes[..4].try_into().unwrap()) as usize;
            bytes = &bytes[4..];

            if bytes.len() < len {
                return Err(Other(format!("no description").into()));
            }
            description = std::str::from_utf8(&bytes[..len])
                .map_err(|_| Other(format!("non-utf8 description").into()))?
                .to_owned();
            bytes = &bytes[len..];
        }

        if bytes.len() > 0 {
            return Err(Other(format!("extra trailing data").into()));
        }

        Ok(Content { title, description })
    }
}

impl<const N: usize> ToSql for HexArray<N> {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput<'_>> {
        self.0.as_ref().to_sql()
    }
}

type Callback<T> = oneshot::Sender<Result<T, Error>>;

#[derive(Debug)]
enum Query {
    Insert(Fragment, Callback<()>),
    Remove(FragId, User, Callback<()>),
    GetAll(String, Callback<Vec<Fragment>>),
    AuthUser(Secret, Callback<Option<User>>),
    CreateAuthToken(Token, Callback<()>),
}

#[derive(Debug, Clone)]
pub struct DbRef {
    tx: mpsc::Sender<Query>,
}

#[derive(Debug)]
// TODO thiserror
pub enum Error {
    Db(rusqlite::Error),
    TokioRuntime(std::io::Error),
    ThreadSpawn(std::io::Error),
    Comm,
    NotFound,
}

impl DbRef {
    fn create_tables_if_absent(conn: &mut Connection) -> Result<(), Error> {
        conn.execute(
            "CREATE TABLE IF NOT EXISTS fragments (
                id              INTEGER PRIMARY KEY,
                realm           TEXT NOT NULL,
                author          INTEGER NOT NULL,
                content         BLOB,
                tags            TEXT,
                date            INTEGER NOT NULL
            )",
            [],
        )
        .map_err(Error::Db)?;

        conn.execute(
            "CREATE TABLE IF NOT EXISTS users (
                id              INTEGER PRIMARY KEY,
                name            TEXT NOT NULL
            )",
            [],
        )
        .map_err(Error::Db)?;

        conn.execute(
            "CREATE TABLE IF NOT EXISTS tokens (
                secret          BLOB NOT NULL,
                user            INTEGER NOT NULL
            )",
            [],
        )
        .map_err(Error::Db)?;

        Ok(())
    }

    fn manage_database(mut rx: mpsc::Receiver<Query>) -> Result<(), Error> {
        let mut db_dir: PathBuf =
            std::env::var("DATABASE_DIR").map_or_else(|_| PathBuf::from("db"), |x| x.into());
        db_dir.push("fragments.sqlite");
        let mut conn = Connection::open(&db_dir).map_err(Error::Db)?;
        Self::create_tables_if_absent(&mut conn)?;

        let mut insert_stmt = conn
            .prepare(
                "INSERT INTO fragments (id, realm, author, content, tags, date)
                 VALUES (?, ?, ?, ?, ?, ?)",
            )
            .map_err(Error::Db)?;
        let mut remove_stmt = conn
            .prepare("DELETE FROM fragments WHERE id = ? AND author = ?")
            .map_err(Error::Db)?;
        let mut getall_stmt = conn
            .prepare(
                "SELECT fragments.id, content, tags, date, users.id, users.name
                 FROM fragments
                 INNER JOIN users ON users.id = fragments.author
                 WHERE realm = ?
                 ORDER BY date",
            )
            .map_err(Error::Db)?;
        let mut getuser_stmt = conn
            .prepare(
                "SELECT id, name FROM users
                INNER JOIN tokens ON users.id = tokens.user
                WHERE secret = ?",
            )
            .map_err(Error::Db)?;
        let mut adduser_stmt = conn
            .prepare(
                "INSERT INTO users (name)
                 SELECT ?
                 WHERE NOT EXISTS (
                     SELECT id FROM users WHERE name = ?
                 )",
            )
            .map_err(Error::Db)?;
        let mut addauthtoken_stmt = conn
            .prepare(
                "INSERT INTO tokens (secret, user)
                 SELECT ?, id FROM users WHERE name = ?",
            )
            .map_err(Error::Db)?;

        let rt = tokio::runtime::Builder::new_current_thread()
            .enable_all()
            .build()
            .map_err(Error::TokioRuntime)?;

        while let Some(query) = rt.block_on(rx.recv()) {
            use Query::*;
            match query {
                Insert(frag, tx) => {
                    let result = insert_stmt
                        .execute(params![
                            &frag.id.0,
                            &frag.realm,
                            &frag.author.id.0,
                            &frag.content,
                            &frag.tags,
                            &frag.date.timestamp_millis()
                        ])
                        .map(|_| ())
                        .map_err(Error::Db);

                    tx.send(result).map_err(|_| Error::Comm)?;
                }

                Remove(id, user, tx) => {
                    let result = remove_stmt
                        .execute([id.0, user.id.0])
                        .map(|_| ())
                        .map_err(Error::Db);

                    tx.send(result).map_err(|_| Error::Comm)?;
                }

                GetAll(realm, tx) => {
                    let mut fetch_data = || {
                        let mut rows = getall_stmt
                            .query_map([&realm], |row| {
                                Ok(Fragment {
                                    id: FragId(row.get(0)?),
                                    realm: realm.clone(),
                                    content: row.get(1)?,
                                    tags: row.get(2)?,
                                    date: {
                                        let millis: i64 = row.get(3)?;
                                        DateTime::from_utc(
                                            NaiveDateTime::from_timestamp(
                                                millis / 1_000,
                                                (millis % 1_000) as u32 * 1_000_000,
                                            ),
                                            Utc,
                                        )
                                    },
                                    author: User {
                                        id: FragId(row.get(4)?),
                                        name: row.get(5)?,
                                    },
                                })
                            })
                            .map_err(Error::Db)?;

                        let mut data = Vec::new();
                        while let Some(frag) = rows.next() {
                            data.push(frag.map_err(Error::Db)?);
                        }
                        Ok(data)
                    };

                    tx.send(fetch_data()).map_err(|_| Error::Comm)?;
                }

                AuthUser(secret, tx) => {
                    let mut find_user = || {
                        let mut iter = getuser_stmt
                            .query_map([&secret], |row| {
                                Ok(User {
                                    id: FragId(row.get(0)?),
                                    name: row.get(1)?,
                                })
                            })
                            .map_err(Error::Db)?;

                        Ok(if let Some(user) = iter.next() {
                            Some(user.map_err(Error::Db)?)
                        } else {
                            None
                        })
                    };

                    tx.send(find_user()).map_err(|_| Error::Comm)?;
                }

                CreateAuthToken(token, tx) => {
                    let execute = || {
                        adduser_stmt
                            .execute(params![&token.username, &token.username])
                            .map(|_| ())
                            .map_err(Error::Db)?;
                        addauthtoken_stmt
                            .execute(params![
                                &token.secret,
                                &token
                                    .username
                                    .expect("database receieved token with None username")
                            ])
                            .map(|_| ())
                            .map_err(Error::Db)?;
                        Ok(())
                    };

                    tx.send(execute()).map_err(|_| Error::Comm)?;
                }
            }
        }

        Ok(())
    }

    pub fn open_database() -> Result<(DbRef, JoinHandle<Result<(), Error>>), Error> {
        let (tx, rx) = mpsc::channel(16);
        let handle = std::thread::Builder::new()
            .name("database".into())
            .spawn(move || Self::manage_database(rx))
            .map_err(Error::ThreadSpawn)?;
        Ok((DbRef { tx }, handle))
    }

    pub async fn insert(&self, frag: Fragment) -> Result<(), Error> {
        let (tx, rx) = oneshot::channel();
        self.tx
            .send(Query::Insert(frag, tx))
            .await
            .map_err(|_| Error::Comm)?;
        rx.await.map_err(|_| Error::Comm)?
    }

    pub async fn remove(&self, id: FragId, user: User) -> Result<(), Error> {
        let (tx, rx) = oneshot::channel();
        self.tx
            .send(Query::Remove(id, user, tx))
            .await
            .map_err(|_| Error::Comm)?;
        rx.await.map_err(|_| Error::Comm)?
    }

    pub async fn get_all(&self, realm: String) -> Result<Vec<Fragment>, Error> {
        let (tx, rx) = oneshot::channel();
        self.tx
            .send(Query::GetAll(realm, tx))
            .await
            .map_err(|_| Error::Comm)?;
        rx.await.map_err(|_| Error::Comm)?
    }

    pub async fn auth_user(&self, secret: Secret) -> Result<Option<User>, Error> {
        let (tx, rx) = oneshot::channel();
        self.tx
            .send(Query::AuthUser(secret, tx))
            .await
            .map_err(|_| Error::Comm)?;
        rx.await.map_err(|_| Error::Comm)?
    }

    pub async fn create_auth_token(&self, token: Token) -> Result<(), Error> {
        let (tx, rx) = oneshot::channel();
        self.tx
            .send(Query::CreateAuthToken(token, tx))
            .await
            .map_err(|_| Error::Comm)?;
        rx.await.map_err(|_| Error::Comm)?
    }
}
