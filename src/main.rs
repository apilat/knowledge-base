mod auth;
mod db;
use auth::{AuthStatus, LoginManager, ReqId, Secret, TOKEN_COOKIE};
use chrono::{DateTime, Utc};
use db::DbRef;
use once_cell::sync::Lazy;
use regex::Regex;
use rocket::{
    form::Form,
    fs::FileServer,
    http::{Cookie, CookieJar, Status},
    request::FromParam,
    response::Redirect,
    serde::json::Json,
    FromForm, State,
};
use rocket_dyn_templates::Template;
use serde::{Deserialize, Serialize, Serializer};
use tokio::sync::Mutex;

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(transparent)]
pub struct FragId(u64);

impl<'a> FromParam<'a> for FragId {
    type Error = <u64 as FromParam<'a>>::Error;
    fn from_param(param: &'a str) -> Result<Self, Self::Error> {
        Ok(FragId(u64::from_param(param)?))
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
#[serde(transparent)]
pub struct Tags(Vec<String>);

impl Tags {
    pub fn valid(&self) -> bool {
        self.0
            .iter()
            .all(|tag| !tag.is_empty() && tag.chars().all(char::is_alphanumeric))
    }
}

#[derive(Debug, Clone, Serialize)]
pub struct User {
    id: FragId,
    name: String,
}

#[derive(Debug, Clone, Serialize)]
pub struct Fragment {
    id: FragId,
    realm: String,
    author: User,
    content: Content,
    tags: Tags,
    #[serde(serialize_with = "serialize_date")]
    date: DateTime<Utc>,
}

pub fn serialize_date<S: Serializer>(
    date: &DateTime<Utc>,
    serializer: S,
) -> Result<S::Ok, S::Error> {
    serializer.serialize_str(&date.format("%Y/%m/%d %H:%M:%S").to_string())
}

impl Fragment {
    pub fn new(realm: String, author: User, content: Content, tags: Tags) -> Self {
        Fragment {
            // Make sure that FragId fits within signed 64-bit int
            id: FragId(rand::random::<u64>() & ((1 << 63) - 1)),
            realm,
            author,
            content,
            tags,
            date: Utc::now(),
        }
    }
}

#[derive(Debug, Clone, Serialize)]
pub struct Content {
    title: String,
    description: String,
}

impl Content {
    fn sanitize(s: &str) -> String {
        let mut escaped = String::new();
        for c in s.chars() {
            match c {
                '&' => escaped.push_str("&amp;"),
                '<' => escaped.push_str("&lt;"),
                '>' => escaped.push_str("&gt;"),
                '"' => escaped.push_str("&quot;"),
                '\'' => escaped.push_str("&#x27;"),
                _ => escaped.push(c),
            }
        }

        const LINK_REGEX: Lazy<Regex> =
            Lazy::new(|| Regex::new(r"https?://[A-Za-z0-9\-_.~!*'();:@&=+$,/?#\[\]]*").unwrap());
        let with_links = LINK_REGEX.replace_all(&escaped, "<a href=\"${0}\">${0}</a>");

        with_links.to_string()
    }

    pub fn new(title: &str, description: &str) -> Self {
        Content {
            title: Self::sanitize(title),
            description: Self::sanitize(description),
        }
    }
}

#[rocket::get("/")]
async fn index(auth: AuthStatus, db: &State<DbRef>) -> Result<Template, Status> {
    #[derive(Serialize)]
    struct State {
        fragments: Vec<Fragment>,
        auth: AuthStatus,
    }
    Ok(Template::render(
        "index",
        &State {
            fragments: if let AuthStatus::User(_) = auth {
                db.get_all("tmp".into())
                    .await
                    .map_err(|_| Status::InternalServerError)?
            } else {
                Vec::new()
            },
            auth,
        },
    ))
}

#[rocket::get("/login")]
async fn login1(login_mgr: &State<Mutex<LoginManager>>) -> Template {
    #[derive(Serialize)]
    struct State {
        req_id: ReqId,
    }
    let req_id = login_mgr.lock().await.start_login();
    Template::render("login", &State { req_id })
}

#[derive(Debug, FromForm)]
struct LoginRequest {
    req_id: ReqId,
    secret: Secret,
    username: String,
}

#[rocket::post("/login", data = "<req>")]
async fn login2(
    req: Form<LoginRequest>,
    cookies: &CookieJar<'_>,
    login_mgr: &State<Mutex<LoginManager>>,
    db: &State<DbRef>,
) -> Redirect {
    let req = req.into_inner();
    if let Ok(secret) = login_mgr
        .lock()
        .await
        .attempt_login(req.req_id, req.secret, req.username, db)
        .await
    {
        cookies.add(
            Cookie::build(TOKEN_COOKIE, secret.to_string())
                .secure(true)
                .http_only(true)
                .permanent()
                .finish(),
        );
        Redirect::to("/")
    } else {
        Redirect::to("/login")
    }
}

#[rocket::post("/logout")]
async fn logout(cookies: &CookieJar<'_>) -> Redirect {
    cookies.remove(Cookie::named(TOKEN_COOKIE));
    Redirect::to("/")
}

#[derive(Debug, Deserialize)]
struct NewRequest {
    // Cannot use reference here since the parser fails when encountering escaped characters.
    title: String,
    description: String,
    tags: Tags,
}

impl NewRequest {
    fn valid(&self) -> bool {
        !self.title.is_empty() && self.tags.valid()
    }
}

#[rocket::post("/fragment/new", data = "<req>")]
async fn new(req: Json<NewRequest>, auth: AuthStatus, db: &State<DbRef>) -> Result<(), Status> {
    let req = req.into_inner();
    if !req.valid() {
        return Err(Status::BadRequest);
    }
    let user = match auth {
        AuthStatus::User(x) => x,
        AuthStatus::Anonymous => return Err(Status::Unauthorized),
    };

    let content = Content::new(&req.title, &req.description);
    let frag = Fragment::new("tmp".into(), user, content, req.tags);
    db.insert(frag)
        .await
        .map_err(|_| Status::InternalServerError)
}

#[rocket::delete("/fragment/<id>")]
async fn delete(id: FragId, auth: AuthStatus, db: &State<DbRef>) -> Result<(), Status> {
    let user = match auth {
        AuthStatus::User(x) => x,
        AuthStatus::Anonymous => return Err(Status::Unauthorized),
    };

    db.remove(id, user)
        .await
        .map_err(|_| Status::InternalServerError)
}

#[tokio::main]
async fn main() {
    let (db_ref, db_thread) = DbRef::open_database().expect("failed to create database thread");
    // TODO occasionally prune login manager and db tokens (todo keep track of unused tokens)
    let login_mgr = Mutex::new(LoginManager::new());

    rocket::build()
        .manage(db_ref)
        .manage(login_mgr)
        .mount(
            "/",
            rocket::routes![index, new, delete, login1, login2, logout],
        )
        .mount("/", FileServer::from("static"))
        .attach(Template::fairing())
        .launch()
        .await
        .expect("fatal server error");

    db_thread
        .join()
        .expect("failed to join database thread")
        .expect("fatal database error");
}
