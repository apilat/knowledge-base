function sendFragment() {
    let form = document.getElementById("form");
    fetch("fragment/new", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
            title: form.title.value,
            description: form.description.value,
            tags: form.tags.value ? form.tags.value.split(',') : [],
        }),
    }).then(resp => {
        if (resp.ok) {
            form.title.value = "";
            form.description.value = "";
            form.tags.value = "";
        }
        location.reload();
    });
}

function deleteFragment(elem) {
    const id = elem.parentElement.dataset.id;
    fetch(`fragment/${id}`, {
        method: "DELETE",
    }).then(_resp => {
        location.reload()
    })
}

function toggleDisplay(elem) {
    let hidden = elem.querySelector(".descriptionHidden");
    let shown = elem.querySelector(".description");

    if (hidden.style.display == "none") {
        hidden.style.display = "";
        shown.style.display = "none";
    } else {
        hidden.style.display = "none";
        shown.style.display = "";
    }
}
